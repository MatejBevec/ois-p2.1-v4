# P2.1 → V4 Odjemalec/strežnik in Node.js

V pripravah na vaje V4 bo predstavljen kratek uvod v **Node.js**:

* **enostavni spletni strežnik** (Hello World; testiranje in ponovni zagon; razvoj lokalno vs. Cloud9; spremenljivke na strežniku),
* **napredni spletni strežnik** (strežnik statičnih spletnih strani; uporaba dodatnih knjižnic; obvladovanje zahtev po virih, ki ne obstajajo; predpomnjenje; dinamične strani; pridobivanje podatkov uporabnika; shranjevanje v datoteko).