// Zaradi združljivosti razvoja na lokalnem računalniku vs. Cloud9 okolju
var port = process.env.PORT || 8080;

// Opredelimo knjižnice, ki jih potrebujemo
var mime = require('mime-types');
var formidable = require('formidable');
var http = require('http');
var fs = require('fs');
var path = require('path');

var predpomnilnik = {};

// Kreiramo objekt strežnika in mu nastavimo osnovne parametre
var streznik = http.createServer(function(zahteva, odgovor) {
  var potDoDatoteke = false;
  
  if (zahteva.url == "/") {
    potDoDatoteke = "./public/index.html";
  } else if (zahteva.url == "/posredujSporocilo") {
    obdelajSporocilo(zahteva, odgovor);
  } else {
    potDoDatoteke = "./public" + zahteva.url;
  }
  
  // Statično vsebino postrežemo le takrat, ko gre za zahtevo takšne vrste
  if (potDoDatoteke)
    posredujStaticnoVsebino(odgovor, predpomnilnik, potDoDatoteke);
});

// Na tej točki strežnik poženemo
streznik.listen(port, function() {
  console.log("Strežnik pognan.");
});

// Metoda za posredovanje statične vsebine (datoteke iz mape public)
function posredujStaticnoVsebino(odgovor, predpomnilnik, absolutnaPotDoDatoteke) {
  // Preverjanje ali je datoteka v predpomnilniku
  if (predpomnilnik[absolutnaPotDoDatoteke]) {
    posredujDatoteko(odgovor, absolutnaPotDoDatoteke, 
      predpomnilnik[absolutnaPotDoDatoteke]);
  } else {
    fs.exists(absolutnaPotDoDatoteke, function(datotekaObstaja) {
      if (datotekaObstaja) {
        fs.readFile(absolutnaPotDoDatoteke, function(napaka, datotekaVsebina) {
          if (napaka) {
            posredujNapako(odgovor, 500);
          } else {
            // Shranjevanje vsebine nove datoteke v predpomnilnik
            predpomnilnik[absolutnaPotDoDatoteke] = datotekaVsebina;
            posredujDatoteko(odgovor, absolutnaPotDoDatoteke, datotekaVsebina);
          }
        });
      } else {
        posredujNapako(odgovor, 404);
      }
    });
  }
}

// Obvladovanje napak
function posredujNapako(odgovor, tip) {
  odgovor.writeHead(tip, {"Content-Type": "text/plain"});
  if (tip == 404) {
    odgovor.end("Napaka 404: Vira ni mogoče najti.");
  } else if (tip == 500) {
    odgovor.end("Napaka 500: Prišlo je do napake strežnika.");
  } else {
    odgovor.end("Napaka " + tip + ": Neka druga napaka.");
  }
}

// Metoda, ki vrne datoteko in nastavi tip datoteke, 
// da jo brskalnik zna ustrezno prikazati
function posredujDatoteko(odgovor, datotekaPot, datotekaVsebina) {
  odgovor.writeHead(200, {"Content-Type": mime.lookup(path.basename(datotekaPot))});
  odgovor.end(datotekaVsebina);
}

// Metoda, ki obdela sporočila na strežniku
function obdelajSporocilo(zahteva, odgovor) {
  var obrazec = new formidable.IncomingForm();
  obrazec.parse(zahteva, function(napaka, vrednosti) {
    if (napaka) {
      posredujNapako(odgovor, 404);
    } else {
      var sporociloUporabnika = new Date().toJSON() + "|" + 
        vrednosti.oseba + "|" + vrednosti.komentar + "|" + vrednosti.strah + "\n";
      fs.appendFile("./sporocila.txt", sporociloUporabnika, function(napaka) {
        if (napaka) {
          posredujNapako(odgovor, 500);
        } else {
          posredujZahvaloZaPosredovaneKomentarje(odgovor);
        }
      });
    }
  });
}

// Metoda, ki se zahvali uporabniku
function posredujZahvaloZaPosredovaneKomentarje(odgovor) {
  odgovor.writeHead(200, {"Content-Type": "text/plain"});
  odgovor.end("Hvala za posredovane komentarje!");
}